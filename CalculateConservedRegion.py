#!/usr/bin/env python3
from collections import Counter

# Calculate per position in the multiple sequence alignment the conservation for that particular position, furthermore
# add the calculated value and the character that is most common to a list and return it.
from Bio import AlignIO


def calculate_conserved_region(msa):
    list_conservation_pos = []
    list_conservation_pos_current = []

    # Call the get_aa_per_position function and pass the msa on. Receive a list that contains smaller lists, that on
    # their part contain the amino acids for one particular position in the multiple sequence alignment.
    list_aa_region, multiple_sequence_alignment = get_aa_per_position(msa)

    index = 0

    while index < len(list_aa_region):
        # Get the character that is most common and the amount of times it occurs.
        most_common, num_most_common = Counter(list_aa_region[index]).most_common(1)[0]

        # Calculate the conservation of a position.
        conservation = num_most_common * 100 / len(multiple_sequence_alignment)

        # Append the most common character and the conservation to a list.
        list_conservation_pos_current.append(most_common)
        list_conservation_pos_current.append(conservation)

        # Append the list above to the a greater list.
        list_conservation_pos.append(list_conservation_pos_current)

        # Empty the list that contains the most_common and conservation for a particular position.
        list_conservation_pos_current = []

        index += 1

    return list_conservation_pos


# Import the AlignIo function from the Bio Python module to generate a multiple sequence object that is easy to iterate
# through. Use that object to get all the amino acids for a particular position, and add them to a list. At last this
# list contains smaller list containing the amino acids for each particular position in the msa. Return that list and
# the msa object.
def get_aa_per_position(msa):
    list_aa_region = []
    list_aa_current = []

    # Use the AlignIO function to generate a object that is easier to iterate through.
    multiple_sequence_alignment = list(AlignIO.read(msa, "clustal"))

    index = 0

    # For each position in the multiple sequence alignment, get that amino acid and add it to a list. Each list contains
    # a amino acids for a particular position from each alignment.
    while index < len(multiple_sequence_alignment[0]):
        # For each sequence get the amino acid on position that is equal to the index.
        for record in multiple_sequence_alignment:
            list_aa_current.append(record[index])

        # Add a list containing the amino acids for a particular position to a greater list.
        list_aa_region.append(list_aa_current)

        # Empty the current list.
        list_aa_current = []
        index += 1

    return list_aa_region, multiple_sequence_alignment
