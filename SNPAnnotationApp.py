#!/usr/bin/env python3
import getopt
import sys
import re
import math
import CheckCMDArguments

from CalculateConservedRegion import calculate_conserved_region
from ParseProteinFile import parse_protein_file

"""
@author = Tom Wagenaar

The SNPAnnotationApp file functions as the main of the application, parsing of the command line arguments, translation 
of the sequences, inserting the SNP in the rna sequences and generating conclusion takes place in this file. In addition 
function from other files are imported to support those actions or provide important data.
"""


# Function that parses the command line arguments.
def parse_cmd_arguments(cmd_arguments):
    for argument, value in cmd_arguments:
        if argument in ('-n', '--nucleotide'):
            # Check if the SNP is a A, U, C or G otherwise return an error message.
            checked_nucleotide = CheckCMDArguments.check_nucleotide(value)
            nucleotide = checked_nucleotide
        elif argument in ('-p', '--position'):
            position = value
        elif argument in ('-m', '--msa'):
            msa = value

    return nucleotide, position, msa


# Function that translate the gene sequence to a rna sequence.
def translate_gen_to_rna(protein_object):
    rna_seq = ''
    codon_holder = ''

    # Retrieve the codon table from the translate_sequence_table function.
    codon_table = translate_sequence_table()

    # For each amino acid in the gen sequence, get the corresponding codon.
    for amino_acid in protein_object.get_sequence():
        for codon, aa in codon_table.items():
            if aa == amino_acid:
                codon_holder = codon
        rna_seq += codon_holder

    return rna_seq


# Function that holds a dictionary that contains the rna codons with corresponding amino acids.
def translate_sequence_table():
    codon_table = {"UUU": "F", "UUC": "F", "UUA": "L", "UUG": "L",
                   "UCU": "S", "UCC": "s", "UCA": "S", "UCG": "S",
                   "UAU": "Y", "UAC": "Y", "UAA": "STOP", "UAG": "STOP",
                   "UGU": "C", "UGC": "C", "UGA": "STOP", "UGG": "W",
                   "CUU": "L", "CUC": "L", "CUA": "L", "CUG": "L",
                   "CCU": "P", "CCC": "P", "CCA": "P", "CCG": "P",
                   "CAU": "H", "CAC": "H", "CAA": "Q", "CAG": "Q",
                   "CGU": "R", "CGC": "R", "CGA": "R", "CGG": "R",
                   "AUU": "I", "AUC": "I", "AUA": "I", "AUG": "M",
                   "ACU": "T", "ACC": "T", "ACA": "T", "ACG": "T",
                   "AAU": "N", "AAC": "N", "AAA": "K", "AAG": "K",
                   "AGU": "S", "AGC": "S", "AGA": "R", "AGG": "R",
                   "GUU": "V", "GUC": "V", "GUA": "V", "GUG": "V",
                   "GCU": "A", "GCC": "A", "GCA": "A", "GCG": "A",
                   "GAU": "D", "GAC": "D", "GAA": "E", "GAG": "E",
                   "GGU": "G", "GGC": "G", "GGA": "G", "GGG": "G"}

    return codon_table


# Function that replaces a nucleotide on a index that is equal to the position that the user provided.
def insert_snp(nucleotide, position, rna_seq):
    position = CheckCMDArguments.check_position(position, rna_seq)

    # Get all the characters before and after the position of the nucleotide that needs to change and add the snp in
    # between.
    rna_seq = rna_seq[:position] + str(nucleotide) + rna_seq[position + 1:]

    return position, rna_seq


# Function that translates the rna sequence to a gen sequence using a codon table, returning the gen sequence with the
# SNP.
def translate_rna_to_gen(rna_seq):
    gen_seq = ''
    aa_holder = ''

    # Retrieve the codon table from the translate_sequence_table function.
    codon_table = translate_sequence_table()

    # Split the rna sequence on every third character, to get all the codons.
    list_codons = re.findall('.{1,3}', rna_seq)

    # For each codon in the rna sequence, get the corresponding amino acid.
    for codon_holder in list_codons:
        for codon, aa in codon_table.items():
            if codon == codon_holder:
                aa_holder = aa
        gen_seq += aa_holder

    return gen_seq


# Function that uses the position / 3 rounded down to get the position of corresponding amino acid.
def get_aa_with_snp(position, gen_seq_with_snp):
    aa_snp_position = math.floor((int(position)) / 3)
    aa_with_snp = gen_seq_with_snp[aa_snp_position]
    return aa_snp_position, aa_with_snp


# Generate a conclusion based on the conservation of the position where the snp is introduced, the conclusion takes
# into account what the conservation of the position of the SNP in the MSA is and if the SNP altered the conformation
# of the amino acid.
def generate_conclusion(list_conservation_pos, aa_snp_position, aa_with_snp, cmd_position):
    list_aa__without_gaps = []

    for position in list_conservation_pos:
        if position[0] != '-':
            list_aa__without_gaps.append(position)

    # Print a conclusion if the amino acid with the SNP init is the same as the amino acid in the MSA.
    if list_aa__without_gaps[aa_snp_position][0] == aa_with_snp:
        print('\nYou have generated a SNP on position: ' + str(cmd_position) +
              ', resulting in a alteration of amino acid ' + str(list_aa__without_gaps[aa_snp_position][0]) +
              ' into amino acid ' + str(aa_with_snp) +
              '.Therefore the amino acid composition is not changed, resulting that the outcome of the SNP = 1 '
              '(neutral).')
    # Print a conclusion if the amino acid with the SNP is not the same as the amino acid in the msa and the
    # conservation of the that position is greater or equal then 90.
    elif list_aa__without_gaps[aa_snp_position][0] != aa_with_snp and list_aa__without_gaps[aa_snp_position][1] >= 90:
        print('\nYou have generated a SNP on position: ' + str(cmd_position) +
              ', resulting in a alteration of amino acid ' + str(list_aa__without_gaps[aa_snp_position][0]) +
              ' into amino acid ' + str(aa_with_snp) +
              '.The conservation on this position = ' + str(list_aa__without_gaps[aa_snp_position][1]) +
              ', therefore this is greater or the same as the conservation criteria of 90%, resulting in a outcome of '
              ' SNP = 10. Meaning that the SNP is deleterious!')
    # Print a conclusion if the amino acid with the SNP is not the same as the amino acid in the msa and the
    #     # conservation of the that position is less then 90.
    elif list_aa__without_gaps[aa_snp_position][0] != aa_with_snp and list_aa__without_gaps[aa_snp_position][1] < 90:
        print('\nYou have generated a SNP on position: ' + str(cmd_position) +
              ', resulting in a alteration of amino acid ' + str(list_aa__without_gaps[aa_snp_position][0]) +
              ' into amino acid ' + str(aa_with_snp) +
              '.The conservation on this position = ' + str(list_aa__without_gaps[aa_snp_position][1]) +
              ', therefore this is smaller then the conservation criteria of 90%. Meaning although the SNP changed the '
              'amino acid composition, the conservation criteria was not met, therefore the change is neutral.')


# The main function of the application, here different functions are called and data is passed on to those functions,
# furthermore different function in other files are used.
def main(argv):
    short_option = 'hn:p:m:'
    long_option = ['help', 'nucleotide=', 'position=', 'msa=']

    # Use Getopt store the command line arguments in variables, and the length of the command line arguments in checked.
    # A error is print whenever one of the arguments isn't know or a value is wrong.
    try:
        cmd_arguments, values = getopt.getopt(argv, short_option, long_option)
        CheckCMDArguments.check_length_cmd(cmd_arguments)
    except getopt.error as err:
        print(str(err))
        print("Please use -h to get information about how to use the application!")
        sys.exit(2)

    # Parsing the command line arguments.
    nucleotide, position, msa = parse_cmd_arguments(cmd_arguments)

    # Calculating the conservation of each position in the multiple sequence alignment.
    list_conservation_pos = calculate_conserved_region(msa)

    # Parse the gene sequence file of interest.
    protein_object = parse_protein_file()

    # Translating the gene sequence to a rna sequence.
    rna_seq = translate_gen_to_rna(protein_object)

    # Inserting the SNP in the rna sequence.
    position_checked, rna_seq_with_snp = insert_snp(nucleotide, position, rna_seq)

    # Translating the rna sequence to a gene sequence.
    gen_sequence_with_snp = translate_rna_to_gen(rna_seq_with_snp)

    # Getting the correct amino acid that contains the SNP.
    aa_snp_position, aa_with_snp = get_aa_with_snp(position_checked, gen_sequence_with_snp)

    # Generating a conclusion back to the user.
    generate_conclusion(list_conservation_pos, aa_snp_position, aa_with_snp, position)


if __name__ == '__main__':
    main(sys.argv[1:])
