#!/usr/bin/env python3
from Protein import Protein

"""
@author = Tom Wagenaar

Parse the protein file that holds the gene of interest.
"""
def parse_protein_file():
    header = ''
    sequence = ''

    with open('Data\CCR5ProteinSeq.txt', 'r') as reader:
        file_lines_list = list(reader)

        for line in file_lines_list:
            if line.startswith('>'):
                header = line
            else:
                sequence += line

        # Remove space bars in the sequence
        sequence = ''.join(sequence.split())

        # Create a protein object.
        protein = Protein(header, sequence)

    return protein
