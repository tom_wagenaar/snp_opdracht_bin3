#!/usr/bin/env python3
import sys

from numpy.core.defchararray import upper

"""
@author = Tom Wagenaar

The function of this file is to check and report back to the user whenever the command line arguments are valid or not.
"""

# Function that returns a help message whenever the user entered -h or when the user provided less then 3 arguments.
def check_length_cmd(cmd_arguments):
    for argument, value in cmd_arguments:
        if argument in ('-h', '--help') or len(cmd_arguments) < 3:
            print('This application can be used to determine if a Single Nucleotide Polymorphism (SNP) is deleterious '
                  'or \nneutral. Type in the command line: python.exe SNPAnnotationApp.py with the following options:\n'
                  '-------------|-----------------------------|------------------------------------------------------\n'
                  'Option short | Option long                 | Explanation \n'
                  '-------------|-----------------------------|------------------------------------------------------\n'
                  '-h           | --help                      | \n'
                  '-n           | --nucleotide                | Turn nucleotide into other nucleotide (A,U,C,G)\n'
                  '-p           | --position                  | Position of the SNP  \n'
                  '-m           | --msa                       | Multiple sequence alignment that you need to provide \n'
                  'An example: -n A -p 10 -m Data\CCR5_MSA.clustal_num ')

            # Stop the application from running any further.
            sys.exit(2)


# Check if the nucleotide is a A, U, C or G, report appropriate error message if that isn't the case.
def check_nucleotide(nucleotide):
    if upper(nucleotide) in ['A', 'U', 'C', 'G']:
        nucleotide_checked = upper(nucleotide)
        print('SNP is a valid nucleotide.')
    else:
        print('SNP is not valid, please choose one of the following nucleotide''s: A, U, C, G')
        sys.exit(2)

    return nucleotide_checked


# Check if the position that the user provided is a number between 0 and the length of the rna sequence.
def check_position(position, rna_seq):
    try:
        position = int(position) - 1

        if position >= len(rna_seq):
            print('Position is greater then the amount of nucleotide''s in the translated gen sequence, please '
                  'provide a number that is smaller then: ' + str(len(rna_seq)))
            sys.exit(2)
        elif position < 0:
            print('Position is either 0 or negative, please provide a greater number then 0!')
            sys.exit(2)

        print('Postion is a valid number!')
    except ValueError as error:
        print('Please provide a number for position!')
        sys.exit(2)

    return position
