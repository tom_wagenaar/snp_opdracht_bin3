#!/usr/bin/env python3
"""
Protein class that is used to store the sequence of the gene of interest.
"""


class Protein:

    def __init__(self, header, sequence):
        self.header = header
        self.sequence = sequence

    def get_header(self):
        return self.header

    def get_sequence(self):
        return self.sequence
